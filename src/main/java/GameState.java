/** Peiqi Yin, A13783252, p4yin@ucsd.edu
 * This file constructs the grid of the game and defines the positions of the
 * pieces on the board.
 * This includes
 *
 * */
import java.util.*;

/** your class header here */
public class GameState {

    // Used to populate char[][] board below and to display the
    // current state of play.
    final static char TRAIL_CHAR = '.';
    final static char OBSTACLE_CHAR = 'X';
    final static char SPACE_CHAR = ' ';
    final static char CURRENT_CHAR = 'O';
    final static char GOAL_CHAR = '@';
    final static char NEWLINE_CHAR = '\n';

    // This represents a 2D map of the board
    char[][] board;

    // Location of the player
    int playerRow;
    int playerCol;

    // Location of the goal
    int goalRow;
    int goalCol;

    // true means the player completed this level
    boolean levelPassed;

    // initialize a board of given parameters, fill the board with SPACE_CHAR
    // set corresponding fields to parameters.
    public GameState(int height, int width, int playerRow, int playerCol,
                     int goalRow, int goalCol) {
        //initialize board
        this.board = new char [height][width];
        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                board[row][col] = SPACE_CHAR;
            }
        }

        //initialize location of the player(bottom left corner)
        this.playerRow = playerRow;
        this.playerCol = playerCol;
        this.board[playerRow][playerCol] = CURRENT_CHAR;

        //initialize location of the goal
        this.goalRow = goalRow;
        this.goalCol = goalCol;
        this.board[goalRow][goalCol] = GOAL_CHAR;

        this.levelPassed = false;
    }

    // copy constructor
    public GameState(GameState other) {
        int width = other.board[0].length;
        int height = other.board.length;
        // make a deep copy to board array
        this.board = new char [width][height];
        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                this.board[row][col] = other.board[row][col];
            }
        }

        this.playerRow = other.playerRow;
        this.playerCol = other.playerCol;
        this.goalRow = other.goalRow;
        this.goalCol = other.goalCol;
        this.levelPassed = other.levelPassed;
    }


    // add count random blocks into this.board
    // avoiding player position and goal position
    void addRandomObstacles(int count) {
        //empty spaces available (total space - 1 goal - 1 snake)
        // optz : calc width and height once
        int width = this.board[0].length;
        int height = this.board.length;
        // optz : directly use calced result to calc total space num
        int space = width * height - 2 ;
        //return if edge cases are met
        if (count > space || count < 0) {
            return;
        }

        // optz : random can be used for multi times
        Random random = new Random();
        int i = 0;
        while (i < count) {
            //generate a random row position from 0-number of rows-1
            int row = random.nextInt(height);
            //generate a random col position from 0-number of cols-1
            int col = random.nextInt(width);

            //check if the position is empty then draw obstacle on top
            if (row != this.playerRow && row != this.goalRow &&
                col != this.playerCol && col!= this.goalCol &&
                this.board[row][col] != OBSTACLE_CHAR) {
                this.board[row][col] = OBSTACLE_CHAR;
                i++;
            }
        }
    }


    // rotate clockwise once
    // rotation should account for all instance var including board, current
    // position, goal position
    void rotateClockwise() {
        int width = this.board[0].length;
        int height = this.board.length;
        char[][] rotatedBoard = new char[width][height];
        for(int i = 0; i < width; i ++) {
            for(int j = 0; j < height; j++) {
                rotatedBoard[i][j] = this.board[height - j - 1][i];
            }
        }
        this.board = rotatedBoard;

        // TODO: updating goal/player position
    }


    // move current position towards right until stopped by obstacle / edge
    // leave a trail of dots for all positions that we're walked through
    // before stopping
    void moveRight() {
        move(Direction.RIGHT);
    }


    // move towards any direction given
    // accomplish this by rotating, move right, rotating back
    void move(Direction direction) {
        boolean keepMoving = true;
        int width = this.board[0].length;
        int height = this.board.length;
        while(keepMoving) {
            switch (direction) {
                case UP:
                    // check if snake can go on before make a real movement
                    if(this.playerRow > 0
                            && this.board[this.playerRow - 1][this.playerCol] != OBSTACLE_CHAR
                            && this.board[this.playerRow - 1][this.playerCol] != TRAIL_CHAR) {
                        this.board[this.playerRow][this.playerCol] = TRAIL_CHAR;
                        this.playerRow = this.playerRow - 1;
                        this.board[this.playerRow][this.playerCol] = CURRENT_CHAR;
                    } else {
                        keepMoving = false;
                    }
                    break;
                case DOWN:
                    if(this.playerRow < (height - 1)
                            && this.board[this.playerRow + 1][this.playerCol] != OBSTACLE_CHAR
                            && this.board[this.playerRow + 1][this.playerCol] != TRAIL_CHAR) {
                        this.board[this.playerRow][this.playerCol] = TRAIL_CHAR;
                        this.playerRow = this.playerRow + 1;
                        this.board[this.playerRow][this.playerCol] = CURRENT_CHAR;
                    } else {
                        keepMoving = false;
                    }
                    break;
                case LEFT:
                    if(this.playerCol > 0
                            && this.board[this.playerRow][this.playerCol - 1] != OBSTACLE_CHAR
                            && this.board[this.playerRow][this.playerCol - 1] != TRAIL_CHAR) {
                        this.board[this.playerRow][this.playerCol] = TRAIL_CHAR;
                        this.playerCol = this.playerCol - 1;
                        this.board[this.playerRow][this.playerCol] = CURRENT_CHAR;
                    } else {
                        keepMoving = false;
                    }
                    break;
                case RIGHT:
                    if(this.playerCol < (width - 1)
                            && this.board[this.playerRow][this.playerCol + 1] != OBSTACLE_CHAR
                            && this.board[this.playerRow][this.playerCol + 1] != TRAIL_CHAR) {
                        this.board[this.playerRow][this.playerCol] = TRAIL_CHAR;
                        this.playerCol = this.playerCol + 1;
                        this.board[this.playerRow][this.playerCol] = CURRENT_CHAR;
                    } else {
                        keepMoving = false;
                    }
                    break;
                default:
                    // should not be here
                    System.err.println("Unknown Direction : " + direction);
            }

        }
        if(this.playerCol == this.goalCol && this.playerRow == this.goalRow) {
            System.out.println("Goal Arrival");
        }
    }


    @Override
    // compare two game state objects, returns true if all fields match
    public boolean equals(Object other) {

        // TODO: check for any conditions that should return false

        // We have exhausted all possibility of mismatch, they're identical
        return true;
    }


    @Override
    public String toString() {
        StringBuilder output = new StringBuilder();
        StringBuilder header = new StringBuilder();
        int width = this.board[0].length;
        int height = this.board.length;
        // two block each side and 2 time width - 1
        int length = width * 2 + 3;
        // generate header and footer strings
        for(int i = 0; i < length; i++) {
            header.append('-');
        }
        header.append(NEWLINE_CHAR);

        // appending header
        output.append(header);

        // drawing lines
        for (int i = 0; i < height; i ++) { //outer loop for rows
            // new line begin with | + space
            output.append('|').append(SPACE_CHAR);

            for (int j = 0; j < length - 3; j++) { //inner loop for columns
                // file in space
                if (j % 2 == 0) {
                    output.append(this.board[i][j/2]);
                } else {
                    output.append(SPACE_CHAR);
                }
            }

            // end with | and line separator
            output.append('|').append(NEWLINE_CHAR);
        }

        // appending footer
        output.append(header);

        return output.toString();
    }
}

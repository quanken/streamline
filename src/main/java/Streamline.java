/** your file header here */

import java.util.*;
import java.io.*;

/** your class header here */
public class Streamline {

    final static int DEFAULT_HEIGHT = 6;
    final static int DEFAULT_WIDTH = 5;

    final static String OUTFILE_NAME = "saved_streamline_game";

    GameState currentState;
    List<GameState> previousStates;

    public Streamline() {
        // TODO
    }

    public Streamline(String filename) {
        try {
            loadFromFile(filename);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    protected void loadFromFile(String filename) throws IOException {
        // TODO
    }

    void recordAndMove(Direction direction) {
        // TODO
    }


    void undo() {
        // TODO
    }


    void play() {
        // TODO
    }


    void saveToFile() {
        /**
        try {
            // TODO: use OUTFILE_NAME as the filename and save

        } catch (IOException e) {
            e.printStackTrace();
        }
         */
    }
}

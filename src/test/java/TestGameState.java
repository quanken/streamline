public class TestGameState {
    public static void main(String[] args) {
        GameState gs = new GameState(6, 5, 5, 0, 0, 4);
        gs.addRandomObstacles(3);
        System.out.println(gs.toString());
        gs.move(Direction.UP);
        gs.move(Direction.DOWN);
        gs.move(Direction.LEFT);
        gs.move(Direction.RIGHT);
        System.out.println(gs.toString());
    }
}
